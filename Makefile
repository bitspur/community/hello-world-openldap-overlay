# File: /Makefile
# Project: hello-world-openldap-overlay
# File Created: 02-09-2021 19:31:29
# Author: Clay Risser <email@clayrisser.com>
# -----
# Last Modified: 05-09-2021 02:41:37
# Modified By: Clay Risser <email@clayrisser.com>
# -----
# Silicon Hills LLC (c) Copyright 2021
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

export SLAPD_MODULE ?= hello_world
export NAME ?= hello-world-openldap-overlay
export REGISTRY ?= registry.gitlab.com/bitspur/community
export VERSION ?= 0.0.1
export NUMPROC ?= $(shell nproc)
export WORKDIR ?= ./

export IMAGE := $(REGISTRY)/$(NAME)

CWD ?= $(shell pwd)
GIT ?= $(shell git --version >/dev/null 2>/dev/null && echo git|| echo true)
SUDO ?= $(shell sudo -h >/dev/null 2>/dev/null && echo sudo|| echo)
USER ?= root

.PHONY: all
all: build

.PHONY: build
build: $(WORKDIR)/debs/slapd.deb

$(WORKDIR)/debs/slapd.deb: $(WORKDIR)/openldap/contrib/slapd-modules/hello_world/Makefile
	@mkdir -p $(WORKDIR)/debs
	@cd $(WORKDIR)/openldap && \
		DEB_BUILD_OPTIONS='nocheck' dpkg-buildpackage -b -uc -us -j$(NUMPROC)
	@mv $(WORKDIR)/*.deb $(WORKDIR)/debs

.PHONY: prepare
prepare: $(WORKDIR)/openldap/contrib/slapd-modules/hello_world/Makefile
$(WORKDIR)/openldap/contrib/slapd-modules/hello_world/Makefile: $(WORKDIR)/openldap/debian/rules $(ls src)
	@rm -rf $(WORKDIR)/openldap/contrib/slapd-modules/hello_world
	@cp -r src $(WORKDIR)/openldap/contrib/slapd-modules/hello_world

$(WORKDIR)/openldap/debian/rules: sudo
	@mkdir -p $(WORKDIR)
	@cd $(WORKDIR) && $(SUDO) apt-get source slapd
	@cd $(WORKDIR) && $(SUDO) chown -R $(USER):$(USER) .
	@cd $(WORKDIR) && mv $$(ls | grep -E "^openldap[^_].*") openldap
	@sed -i 's|^CONTRIB_MODULES = |CONTRIB_MODULES = $(SLAPD_MODULE) |g' $(WORKDIR)/openldap/debian/rules
	@echo "usr/lib/ldap/$(SLAPD_MODULE).so*" >> $(WORKDIR)/openldap/debian/slapd.install
	@echo "usr/lib/ldap/$(SLAPD_MODULE).la" >> $(WORKDIR)/openldap/debian/slapd.install

.PHONY: install
install: sudo build
	@$(SUDO) (dpkg -i debs/*.deb || true) && \
		$(SUDO) apt-get update && \
		$(SUDO) apt-get install -f -y && \
		$(SUDO) dpkg -i debs/*.deb

.PHONY: sudo
	@$(SUDO) true

.PHONY: clean
clean: docker-clean
	-@$(GIT) clean -fXd

.PHONY: purge
purge: clean

.PHONY: docker-%
docker-%:
	@$(MAKE) -s -f docker.mk $(shell echo $@ | sed "s/docker-//") ARGS=$(ARGS)

.PHONY: up ssh logs
up: docker-up
ssh: docker-ssh
logs: docker-logs

.PHONY: start
start: docker-clean docker-build
	@$(MAKE) -s up ARGS="-d"
	@$(MAKE) -s logs

.PHONY: deps
deps: sudo
	@$(SUDO) apt-get install -y \
		apt-utils \
		build-essential \
		dpkg-dev
	@$(SUDO) apt-get build-dep slapd -y

.PHONY: ~%
~%:
	@$(MAKE) -s $(shell echo $@ | $(SED) 's/^~//g') ARGS="-d"

.PHONY: %
%: ;
