# File: /context/entrypoint.sh
# Project: hello-world-openldap-overlay
# File Created: 03-09-2021 18:08:29
# Author: Clay Risser <email@clayrisser.com>
# -----
# Last Modified: 04-09-2021 04:14:04
# Modified By: Clay Risser <email@clayrisser.com>
# -----
# Silicon Hills LLC (c) Copyright 2021
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

__ldapadd() {
    ldapadd -c -Y EXTERNAL -H ldapi:/// -f $1 2>&1 | \
        tee /tmp/ldapadd.log
    cat /tmp/ldapadd.log | grep -q "Can't contact LDAP server"
    if [ "$?" = "0" ]; then
        false
    fi
}

__load_ldif() {
    mkdir -p /container/ldif
    CWD=$(pwd)
    cd /container/service/slapd/assets/config/bootstrap/ldif
    for f in $(find . -type f -name '*.ldif' | sed 's|^\.\/||g'); do
        mkdir -p $(echo "/container/ldif/${f}" | sed 's|\/[^\/]*$||g')
        cp "/container/service/slapd/assets/config/bootstrap/ldif/${f}" "/container/ldif/${f}"
    done
    cd $CWD
    for f in $(find /container/ldif -type f -name '*.ldif'); do
        until __ldapadd $f
        do
            echo trying ldapadd again in 5 seconds . . .
            sleep 5
        done
    done
    echo LOADED MODULES:
    slapcat -n 0 | grep olcModuleLoad
}

__load_ldif &

exec /container/tool/run $@
