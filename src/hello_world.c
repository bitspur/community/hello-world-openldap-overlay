/**
 * File: /src/hello_world.c
 * Project: hello-world-openldap-overlay
 * File Created: 02-09-2021 19:31:29
 * Author: Clay Risser <email@clayrisser.com>
 * -----
 * Last Modified: 04-09-2021 21:28:25
 * Modified By: Clay Risser <email@clayrisser.com>
 * -----
 * Silicon Hills LLC (c) Copyright 2021
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * -----
 *
 * this is an openlap overlay that is used as an example
 * for building openldap overlays
 *
 * the following is a list of abbreviations used by openldap
 * in the code that could be challenging to understand or look up
 * AD: attribute description
 * ATTR: attribute
 * BD: backend
 * BE: backend database (not exactly sure why be is used to refer to the backend database)
 * BER: basic encoding rule - https://ldap.com/ldapv3-wire-protocol-reference-asn1-ber
 * BV: binary value
 * DESC: description
 * E: entry
 * OP: operation
 * RS: slap reply (not exactly sure why, but notice that the abbreviation is reversed)
 * STR: string
 */

// comment out to disabled debug() calls
#define DEBUG_HELLO_WORLD

#include "portable.h"

#ifndef SLAPD_OVER_HELLO_WORLD
#define SLAPD_OVER_HELLO_WORLD SLAPD_MOD_DYNAMIC
#endif

#ifdef DEBUG_HELLO_WORLD
#define debug(...) Debug(__VA_ARGS__)
#else
#define debug(...)
#endif

#ifdef SLAPD_OVER_HELLO_WORLD

#include <ac/errno.h>
#include <ac/socket.h>
#include <ac/string.h>
#include <slap.h>
#include <stdio.h>

static slap_overinst hello_world;

/**
 * VIRTUAL ATTRIBUTES
 * calculated attributes that are not stored in the database
 *
 * `AttributeDescription` must be declared for
 * each virtual attribute
 */
static AttributeDescription *displayname_ad;

/**
 * SOURCE ATTRIBUTES
 * attributes sourced from the database
 *
 * `AttributeDescription` must be declared for
 * each virtual attribute and a comma separated list of each
 * attribute must be provided in the `SOURCE_ATTRS` variable
 */
static const char *SOURCE_ATTRS = "givenName,sn";
static AttributeDescription *givenname_ad;
static AttributeDescription *sn_ad;

/**
 * gets an attribute from an entry
 */
static struct berval *_get_attr(Entry *entry, AttributeDescription *attr_desc)
{
    const Attribute *attr = attr_find(entry->e_attrs, attr_desc);
    return attr ? &attr->a_vals[0] : NULL;
}

/**
 * adds the virtual attributes to the search response
 * it is important to make this as performant as possible
 * because it is invoked on every return from ldap to the client
 * this is where the virtual attributes are computed
 */
static int hello_world_response(Operation *op, SlapReply *rs)
{
    debug(LDAP_DEBUG_ANY, "hello_world: hello_world_response\n", "", "", 0);

    // skip virtual attributes if an admin is making the request
    // TODO: add configuration to control if virtual attributes apply to admins
    if (be_isroot(op))
    {
        debug(LDAP_DEBUG_ANY, "hello_world: be_isroot(op): true\n", "", "", 0);
        // return SLAP_CB_CONTINUE;
    }

    // skip if this is not a search operation
    // attributes are typically received from a search operatoion
    // TODO: improve comparison for other scenarios
    if (rs->sr_type != REP_SEARCH)
    {
        debug(LDAP_DEBUG_ANY, "hello_world: rs->sr_type != REP_SEARCH: true\n", "", "", 0);
        return SLAP_CB_CONTINUE;
    }

    // setup the overlay response
    const slap_overinst *on = (slap_overinst *)op->o_bd->bd_info;
    op->o_bd->bd_info = (BackendInfo *)on->on_info;

    // copy the entry instead of directly modifying it
    // because it may live in a cache
    (void)rs_entry2modifiable(op, rs, on);
    const Entry *entry = rs->sr_entry;

    // source the attributes
    struct berval *givenname_bv = _get_attr(entry, givenname_ad);
    struct berval *sn_bv = _get_attr(entry, sn_ad);
    if (!givenname_bv && !sn_bv)
    {
        debug(LDAP_DEBUG_ANY, "hello_world: !givenname_bv && !sn_bv\n", "", "", 0);
        return SLAP_CB_CONTINUE;
    }

    // initialize the virtual attribute
    const int hello_len = (givenname_bv ? givenname_bv->bv_len : 0) + (sn_bv ? sn_bv->bv_len : 0) + ((givenname_bv && sn_bv) ? 1 : 0);
    char *hello = ch_malloc(hello_len + 1);
    hello[0] = '\0';

    if (givenname_bv)
    {
        debug(LDAP_DEBUG_ANY, "hello_world: givenname_bv: %s\n", givenname_bv->bv_val, "", 0);
        strncat(hello, givenname_bv->bv_val, givenname_bv->bv_len);
    }
    if (sn_bv)
    {
        debug(LDAP_DEBUG_ANY, "hello_world: sn_bv: %s\n", sn_bv->bv_val, "", 0);
        if (givenname_bv)
        {
            strcat(hello, " ");
        }
        strncat(hello, sn_bv->bv_val, sn_bv->bv_len);
    }

    debug(LDAP_DEBUG_ANY, "hello_world: hello: %s\n", hello, "", 0);

    // add or replace the attribute in the reply entry
    // notice that you have to convert the value to a
    // ber (basic encoding rule) bv (binary value)
    struct berval hello_bv;
    (void)ber_str2bv(hello, hello_len, 0, &hello_bv);
    (void)attr_delete(&entry->e_attrs, displayname_ad);
    (void)attr_merge_normalize_one(entry, displayname_ad, &hello_bv, op->o_tmpmemctx);

    debug(LDAP_DEBUG_ANY, "hello_world: created displayName virtual attribute\n", "", "", 0);

    return SLAP_CB_CONTINUE;
}

/**
 * adds the attributes found in `SOURCE_ATTRS` to the operation so the
 * source attributes are available
 */
static int hello_world_search(Operation *op, SlapReply *rs)
{
    debug(LDAP_DEBUG_ANY, "hello_world: hello_world_search\n", "", "", 0);

    // skip if this is our own internal search
    if (op->o_no_schema_check)
    {
        return SLAP_CB_CONTINUE;
    }
    if (op->ors_attrs)
    {
        // if a requested attribute contains one of our virtual attributes
        // then add the `SOURCE_ATTRS` to the operation
        // right now, this logic is not dynamic, so make sure you check
        // every virtual attribute
        // TODO: make this logic dynamic
        if (an_find(op->ors_attrs, &displayname_ad->ad_cname))
        {
            debug(LDAP_DEBUG_ANY, "hello_world: found attribute %s\n", (&displayname_ad->ad_cname)->bv_val, "", 0);

            str2anlist(op->ors_attrs, SOURCE_ATTRS, ",");
        }
    }
    return SLAP_CB_CONTINUE;
}

/**
 * cleanup a database call
 * use this to deallocate memory
 */
static int hello_world_db_destroy(BackendDB *be, ConfigReply *cr)
{
    debug(LDAP_DEBUG_ANY, "hello_world: hello_world_db_destroy\n", "", "", 0);

    return LDAP_SUCCESS;
}

/**
 * runs when database call opens
 * this runs right after bi_db_init
 */
static int hello_world_db_open(BackendDB *be, ConfigReply *cr)
{
    debug(LDAP_DEBUG_ANY, "hello_world: hello_world_db_open\n", "", "", 0);

    return LDAP_SUCCESS;
}

/**
 * initialize a database call
 * use this to initialize the attribute descriptions
 * that were declared at the top of this file
 * the attribute descriptions will be used in the bi_op_search
 * hook to access and set attributes
 */
static int hello_world_db_init(BackendDB *be, ConfigReply *cr)
{
    debug(LDAP_DEBUG_ANY, "hello_world: hello_world_db_init\n", "", "", 0);

    static struct
    {
        const char *name;
        AttributeDescription **adp;
    } ads[] = {
        // map of ads to initialize
        {"displayName", &displayname_ad},
        {"givenName", &givenname_ad},
        {"sn", &sn_ad},
        {NULL}};

    // loops over ads and initializes them
    int i, rc;
    for (i = 0; ads[i].name != NULL; i++)
    {
        const char *err_msg;
        *(ads[i].adp) = NULL;
        rc = slap_str2ad(ads[i].name, ads[i].adp, &err_msg);
        if (rc != LDAP_SUCCESS)
        {
            Debug(LDAP_DEBUG_ANY, "hello_world: unable to find \"%s\" attributeType: %s (%d)\n",
                  ads[i].name, err_msg, rc);
            return rc;
        }
        debug(LDAP_DEBUG_ANY, "hello_world: initialized ad %s\n", ads[i].name, "", 0);
    }

    return LDAP_SUCCESS;
}

/**
 * initialize the module
 * attach functions to hooks that you want intercepted
 */
int hello_world_initialize()
{
    debug(LDAP_DEBUG_ANY, "hello_world: hello_world_initialize\n", "", "", 0);

    hello_world.on_bi.bi_type = "hello_world";
    hello_world.on_bi.bi_db_destroy = hello_world_db_destroy;
    hello_world.on_bi.bi_db_init = hello_world_db_init;
    hello_world.on_bi.bi_db_open = hello_world_db_open;
    hello_world.on_bi.bi_op_search = hello_world_search;
    hello_world.on_response = hello_world_response;
    return overlay_register(&hello_world);
}

#if SLAPD_OVER_HELLO_WORLD == SLAPD_MOD_DYNAMIC
int init_module(int argc, char *argv[])
{
    debug(LDAP_DEBUG_ANY, "hello_world: init_module\n", "", "", 0);

    return hello_world_initialize();
}
#endif // SLAPD_OVER_HELLO_WORLD == SLAPD_MOD_DYNAMIC

#endif // SLAPD_OVER_HELLO_WORLD
