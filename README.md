# hello-world-openldap-overlay

> an example overlay for openldap

Right now this is only setup to compile on a
Debian based system.

## Usage

### Prepare system

#### Debian

If you are using a Debian based system like Ubuntu, you can install the
require dependencies by running the following command.

```sh
sudo apt install -y make
make deps
```

### Build

```sh
make
```

### Install

1. install

   ```sh
   make install
   ```

2. create _hello_world.ldif_

   ```ldif
   # loads the hello_world module
   dn: cn=module{0},cn=config
   changetype: modify
   add: olcModuleLoad
   olcModuleLoad: hello_world

   # activates the hello_world overlay
   dn: olcOverlay={2}hello_world,olcDatabase={1}mdb,cn=config
   objectClass: olcOverlayConfig
   objectClass: olcPPolicyConfig
   olcOverlay: {2}hello_world
   ```

3. apply the ldif modifications

   ```sh
   ldapadd -c -Y EXTERNAL -H ldapi:/// -f hello_world.ldif
   ```

### Test

You can test this by building the docker image and running it
with docker-compose

```sh
make start
```

### Docker

You can compile into the osixia openldap docker image

```Dockerfile
FROM osixia/openldap:1.5.0 as builder

ENV DEBIAN_FRONTEND=noninteractive

WORKDIR /tmp

RUN echo "deb-src http://deb.debian.org/debian buster main" >> /etc/apt/sources.list && \
    echo "deb-src http://security.debian.org/debian-security buster/updates main" >> /etc/apt/sources.list && \
    echo "deb-src http://deb.debian.org/debian buster-updates main" >> /etc/apt/sources.list && \
    echo "deb-src http://ftp.debian.org/debian buster-backports main" >> /etc/apt/sources.list
RUN apt-get update && apt-get install -y \
    apt-utils \
    build-essential \
    curl \
    dpkg-dev \
    make
RUN apt-get build-dep slapd -y

RUN mkdir -p debs
RUN curl -L -o hello_world.tar.gz \
        https://gitlab.com/bitspur/community/hello-world-openldap-overlay/-/archive/main/hello-world-openldap-overlay-main.tar.gz && \
    tar -xzvf hello_world.tar.gz && \
    mv hello-world-openldap-overlay-main hello_world
RUN cd hello_world && \
    make -s install && \
    mv debs/* ../debs

FROM osixia/openldap:1.5.0

COPY --from=builder /tmp/debs /tmp/debs
RUN apt-get update && \
    (dpkg -i /tmp/debs/*.deb || true) && \
    apt-get install -f -y && \
    dpkg -i /tmp/debs/*.deb && \
    apt-get clean && \
    rm -rf /tmp/debs /var/lib/apt/lists/*
```
